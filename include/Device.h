#ifndef DEVICE_H
#define DEVICE_H

#include "Buzzer.h"

class Device
{
    public:
        Device();
        virtual ~Device();

        Buzzer* buzzer;
};

extern Device device;
#endif // DEVICE_H
