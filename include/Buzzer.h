#ifndef BUZZER_H
#define BUZZER_H

#include <stdio.h>
#include <stdlib.h>

class Buzzer
{
    public:
        Buzzer();
        virtual ~Buzzer();

        void Beep();

    protected:

    private:
};

#endif // BUZZER_H
